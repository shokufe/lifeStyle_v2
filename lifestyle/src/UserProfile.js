import React, { Component } from 'react';
import './css/my_style.css';
import axios from 'axios'
import './connexion.css'
import a from './img/avatar/sh.jpg'
import 'react-notifications-component/dist/theme.css'
import { store } from 'react-notifications-component';
import 'animate.css';
import Event from './Event'
import { Card, CardHeader,CardBody,   Button, UncontrolledCollapse,   Container, Row,  Col  } from "reactstrap";

/**
 *  * @author Shokoufeh AHMADI SIMAB -- V2
 */
class UserProfile extends Component {

    constructor(props){
        super(props)
        this.state={userP_id:null,nom:null, prenom:null, sexe:null, age:null, mail:null, phoneNumber:null, poids:null , taille:null,bmi:null,adress:null,
            ZipCode:null ,country:null, listUserEvent: [],listJointEvent:[]  ,
             selectedFile: null, FriendNum:"0" ,Friendship: "no" ,CeatedEvent:"0",
             JointEvent:"0", MyAnalys: null, Status:null,description2:null, edit:null, 
             scaleValue:1, FriendNum:"0" ,CeatedEvent:"0",JointEvent:"0"}
        //this.followUser =this.followUser.bind(this)
        this.getUserInfo=this.getUserInfo.bind(this)
        this.getUserInfo()
        this.followUser=this.followUser.bind(this)
        this.unfollowUser2=this.unfollowUser2.bind(this)
        this.CountFriends()
        this.CountFriends=this.CountFriends.bind(this)
        this.CountCreatedEvent()
        this.CountCreatedEvent=this.CountCreatedEvent.bind(this)
        this.CountJointEvent()
        this.CountJointEvent=this.CountJointEvent.bind(this)
        this.EventList =this.EventList.bind(this)
        this.EventList()
    }

    getUserInfo(){
        var code
        var url= new URLSearchParams();
        url.append('login', this.props.userP)
        url.append('key',this.props.cle)
        axios.get('http://localhost:8080/Life_Style/GetUserProfile?'+url)
                .then(response => {
                    code=response.data["code"];
                    if(code === 200){
                        this.setState({userP_id:response.data['user_id'],nom: response.data['nom'],prenom: response.data['prenom'],sexe: response.data['sexe'],
                            mail: response.data['mail'],phoneNumber: response.data['phoneNumber']
                                ,adress:response.data['adress'] , country : response.data['Country'], age:response.data['age'] ,Friendship: response.data['Friendship']})
            
                    }
        })
}
followUser(){
    var code
    var url= new URLSearchParams();
    url.append('key', this.props.cle)
    url.append('id_friend',this.props.userP)
    axios.get('http://localhost:8080/Life_Style/AddFriend?'+url)
			.then(response => {
                code=response.data["code"];
				if(code === 200){
             store.addNotification({
              title: "Add successful!",
              message: "You have new friend now",
              type: 'success',
              insert: "top",
              container: "top-right",
              animationIn: ["animated", "fadeIn"],
              animationOut: ["animated", "fadeOut"],
              dismiss: {
                duration: 2000,
                onScreen: true
              }
            });
            this.setState({Friendship:"yes"})
            this.getUserInfo()
            this.props.getUserProfile()
            this.setState({content:"  "})
                }
				else if(code === 2){
           store.addNotification({
            title: "Not allowed!",
            message: "You are already friends",
            type: 'warning',
            insert: "top",
            container: "top-right",
            animationIn: ["animated", "fadeIn"],
            animationOut: ["animated", "fadeOut"],
            dismiss: {
                duration: 2000,
                onScreen: true
           }
         });
				}
				else if(code === 1000){
          store.addNotification({
            title: "Not allowed!",
            message: "this user not exists anymore ",
            type: 'warning',
            insert: "top",
            container: "top-right",
            animationIn: ["animated", "fadeIn"],
            animationOut: ["animated", "fadeOut"],
            dismiss: {
              duration: 2000,
              onScreen: true
            }
          });
				}
			}) 
}
unfollowUser2(){
    var code
    var url= new URLSearchParams();
    url.append('key', this.props.cle)
    url.append('id_friend',this.props.userP)
    axios.get('http://localhost:8080/Life_Style/RemoveFriend?'+url)
                .then(response => {
                    code=response.data["code"];
                    if(code === 200){
                        store.addNotification({
                            title: "Successful!",
                            message: "you remove this friends successfully",
                            type: 'success',
                            insert: "top",
                            container: "top-right",
                            animationIn: ["animated", "fadeIn"],
                            animationOut: ["animated", "fadeOut"],
                            dismiss: {
                            duration: 2000,
                            onScreen: true
                            }
                        });
                            this.setState({Friendship:"no"})
                            this.getUserInfo()
                            this.props.getUserProfile()
                            this.setState({content:"  "})
                            }
                            else if(code === -1){
                                //alert("Pas d'arguments")
                                store.addNotification({
                                    title: "Error!",
                                    message: "Not possible to remove your friend now",
                                    type: 'danger',
                                    insert: "top",
                                    container: "top-right",
                                    animationIn: ["animated", "fadeIn"],
                                    animationOut: ["animated", "fadeOut"],
                                    dismiss: {
                                    duration: 2000,
                                    onScreen: true
                                    }
                                });
                            }
                            else if(code === 1){
                                store.addNotification({
                                    title: "Error!",
                                    message: "you are not connected",
                                    type: 'danger',
                                    insert: "top",
                                    container: "top-right",
                                    animationIn: ["animated", "fadeIn"],
                                    animationOut: ["animated", "fadeOut"],
                                    dismiss: {
                                    duration: 2000,
                                    onScreen: true
                                    }
                                });
                    }
                })   
}    
CountFriends(){
    var url= new URLSearchParams();
    url.append('key',this.props.cle)
    axios.get('http://localhost:8080/Life_Style/FriendCount?'+url)
        .then(response => {
            this.setState({ FriendNum:response.data['FriendNum'] })
  
        
        })   

}
/**
* récupère nombre des Event creer par lui
*/
CountCreatedEvent(){
    var url= new URLSearchParams();
    url.append('key',this.props.cle)
    axios.get('http://localhost:8080/Life_Style/CountCeatedEvent?'+url)
        .then(response => {    
                    this.setState({ CeatedEvent:response.data['CeatedEvent'] })
        })   

}
/**
* récupère nombre des Event qu'il est joints
*/
CountJointEvent(){
    var url= new URLSearchParams();
    url.append('key',this.props.cle)
        axios.get('http://localhost:8080/Life_Style/JointCount?'+url)
              .then(response => {
            console.log(response.data)       
            this.setState({ JointEvent:response.data['JointEvent'] })
        })   
}
EventList(){
    console.log("EventsData");
    var url= new URLSearchParams();
    url.append('key',this.props.cle)
    url.append('userId',this.props.userP)
    axios.get('http://localhost:8080/Life_Style/ListUserEvents?'+url)
        .then(response => {
            console.log("response ", response.data)
            if(response.data.length >=0)
            {
                this.setState({listUserEvent: Object.values(response.data)})
                console.log (this.state.listUserEvent)
                console.log ("length  ",this.state.listUserEvent.length)
            }
            else {
                alert("Pas d'arguments !!")
            }

        })  

}
render(){
    var l = this.state.listUserEvent 
        const map2 = l.map(item =>  <Event
                            user_id={item["id"]} 
                            event_id ={item["event_id"]}
                            user_id={item["user_id"]} 
                            owner={item["owner"]}  
                            name= {item["name"]}
                            date={item["date"]}
                            lieu={item["lieu"]}
                            category={item["category"]}
                            getHome ={this.props.getHome}
                            created_at={item["created_at"]}
                            login = {this.props.login}
                            cle = {this.props.cle}
                                    />
                             )
   if(this.state.Friendship === "no")
    {
        return(
            <Card style={{marginLeft:'0',marginRight:'0',paddingLeft:'0',paddingRight:'0'}}>  
             <Col className="order-xl-2 mb-5 mb-xl-0">
                <Card style={{marginLeft:'0',marginRight:'0',paddingLeft:'0',paddingRight:'0'}} className="backImg">
                 <div class="parallax-bg"></div>
                    <CardBody className="pt-0 pt-md-4"  >
                        <Row style={{marginLeft:'350px',marginRight:'350px' , opacity:'0.8'}}>
                        <div className="col" >
                            <div className="card-profile-stats d-flex justify-content-center mt-md-5">
                            <div className="col numPlace"style={{backgroundColor:'#E87C5E', textAlign:'center',borderRadius:'50%',minHeight:'100px' }}>
                                <div className="heading " style={{marginTop:'15px'}}>{this.state.FriendNum}</div>
                                <div className="description">Friends </div>
                            </div>
                            <div className="col numPlace" style={{backgroundColor:'#E87C5E', textAlign:'center',borderRadius:'50%' }}>
                                <div className="heading "style={{marginTop:'15px'}}>{this.state.CeatedEvent}</div>
                                <div className="description">CreateEvent </div>
                            </div>
                            <div className="col numPlace" style={{backgroundColor:'#E87C5E', textAlign:'center',borderRadius:'50%' }}>
                                <div className="heading "style={{marginTop:'15px'}}>{this.state.JointEvent}</div>
                                <div className="description">JointEvent </div>
                            </div>
                            </div>
                        </div>
                        </Row>
                        <Row style={{backgroundColor:"#A3CBE1",textAlign:'center' ,marginLeft:'auto',marginRight:'auto' , opacity:'0.8'}}>
                            <div className="text-center" style={{ marginLeft:'auto',marginRight:'auto',textAlign:'center',fontFamily:'\"Times New Roman\", Times, serif',color:'#23305B'}}>
                                <div className="h5 font-weight-300" style={{marginTop:'50px',fontWeight:'bold'}}>
                                    My name is {this.state.prenom}{this.state.nom} 
                                </div>
                                <div className="h5 mt-4">
                                    <i className="ni business_briefcase-24 mr-2" />
                                    <p>I 'm from {this.state.country}</p>
                                </div>
                            </div>
                            <button type="button" className="btn btn-outline-success" onClick={()=> this.followUser()} >Follow</button>
                       
                        </Row>
                    </CardBody>
                </Card>
             </Col>
            </Card>
        )
        }
        else if(this.state.Friendship==="yes"){
           return(
            <Card style={{marginLeft:'0',marginRight:'0',paddingLeft:'0',paddingRight:'0'}}>  
            <Col className="order-xl-2 mb-5 mb-xl-0">
               <Card style={{marginLeft:'0',marginRight:'0',paddingLeft:'0',paddingRight:'0'}} className="backImg">
                <div class="parallax-bg"></div>
                   <CardBody className="pt-0 pt-md-4"  >
                       <Row style={{marginLeft:'350px',marginRight:'350px' , opacity:'0.8'}}>
                            <div className="col" >
                                <div className="card-profile-stats d-flex justify-content-center mt-md-5">
                                <div className="col numPlace"style={{backgroundColor:'#E87C5E', textAlign:'center',borderRadius:'50%',minHeight:'100px' }}>
                                    <div className="heading " style={{marginTop:'15px'}}>{this.state.FriendNum}</div>
                                    <div className="description">Friends </div>
                                </div>
                                <div className="col numPlace" style={{backgroundColor:'#E87C5E', textAlign:'center',borderRadius:'50%' }}>
                                    <div className="heading "style={{marginTop:'15px'}}>{this.state.CeatedEvent}</div>
                                    <div className="description">CreateEvent </div>
                                </div>
                                <div className="col numPlace" style={{backgroundColor:'#E87C5E', textAlign:'center',borderRadius:'50%' }}>
                                    <div className="heading "style={{marginTop:'15px'}}>{this.state.JointEvent}</div>
                                    <div className="description">JointEvent </div>
                                </div>
                                </div>
                            </div>
                       </Row>
                       <Row style={{backgroundColor:"#A3CBE1",textAlign:'center' ,marginLeft:'auto',marginRight:'auto' , opacity:'0.8'}}>
                           <div className="text-center" style={{ marginLeft:'auto',marginRight:'auto',textAlign:'center',fontFamily:'\"Times New Roman\", Times, serif',color:'#23305B'}}>
                               <div className="h5 font-weight-300" style={{marginTop:'50px',fontWeight:'bold'}}>
                                   Hi my friend: <br/>
                                   My name is {this.state.prenom}{this.state.nom} 
                               </div>
                               <div className="h5 mt-4">
                                   <i className="ni business_briefcase-24 mr-2" />
                                   <p>my email is: {this.state.mail}</p>
                               </div>
                               <div>My date of birth is: {this.state.age}</div>
                           </div>
                           
                           <button type="button" className="btn btn-outline-success" onClick={()=> this.unfollowUser2()} >UnFollow</button>
                       </Row>
                       <CardBody className="pt-0 pt-md-4">
                            <div className=" backImg row fix_div row-cols-md-2">
                                {map2}
                            </div>
                       </CardBody>
                   </CardBody>
               </Card>
            </Col>
           </Card>
           )  
        }
    
}
}
export default UserProfile;