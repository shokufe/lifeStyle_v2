import React, { Component } from 'react';
import './connexion.css'
import Accueil from "./Accueil";
import Login from "./Login";
import Signup from './Signup';

/**
 * 
 * @author Shokufeh AHMADI SIMABE -- Nassima GHOUT--v1
 * @author Shokufeh AHMADI SIMABE --v2
 */
class MainPage extends Component {
    componentDidMount(){
        document.title = "LifeStyle"
      }
    constructor(props){
        super(props);
        this.state={page:"connexion", isConnected:false, inscrit: true, cle:"", login:null, UserProfileShow:false ,userP:null}
        this.getConnected = this.getConnected.bind(this);
        this.setLogout = this.setLogout.bind(this);
        this.getLogin = this.getLogin.bind(this)
        this.setLogin = this.setLogin.bind(this)
        this.getKey = this.getKey.bind(this)
        this.sInscrire = this.sInscrire.bind(this);
        this.getProfile = this.getProfile.bind(this)
        this.getUserProfile = this.getUserProfile.bind(this)
        this.getAccueil = this.getAccueil.bind(this)
        this.getUsers = this.getUsers.bind(this)
        this.getHome = this.getHome.bind(this)
        this.getFriends = this.getFriends.bind(this)
        this.getInscrit = this.getInscrit.bind(this)
        this.setKey = this.setKey.bind(this)
        this.getCreateEvent = this.getCreateEvent.bind(this)
        this.getExercises = this.getExercises.bind(this)
        
    }
    getConnected(){       
        this.setState(  {
            isConnected:true, page:"Accueil"}); 
    }
    setKey(value){
        this.setState({
            cle: value
        });
    }
    getKey(){
        return (this.state.cle)
    }

    setLogout(){
        this.setState({
            isConnected:false, page:"Connexion"});
    }
    sInscrire(){
        this.setState({inscrit : false, page:"Inscription"});
    }
    getInscrit(){
        this.setState({inscrit : true});
    }
    
    getAccueil(){
        this.setState({page: "Accueil"});
    }
    getUsers(){
        this.setState({page: "ListUsers"});
    }
    getFriends(){
        this.setState({page: "Friends"});
    }
    getProfile(){
        this.setState({page: "Profile"});
    }
    getUserProfile(idUser){
        this.setState({page: "UserProfile",userP:idUser});
    }
    getCreateEvent(){
        this.setState({page: "CreateEvent"})
    }
    getExercises(){
        this.setState({page: "Exercises"})
    }
    getHome(){
        this.setState({page: "Home"});
    }
    setLogin(log){
        this.setState({login: log})
    }
    getLogin(){
         return (this.state.login);
    }


      render() {
        var page;
        if(this.state.inscrit && !this.state.isConnected ){
            page = <Login getConnected={this.getConnected} 
                          sInscrire={this.sInscrire} 
                          getInscrit={this.getInscrit}
                          getProfile ={this.getProfile}
                          cle={this.state.cle}
                          getKey={this.getKey}
                          setKey={this.setKey}
                          login= {this.state.login}
                          setLogin={this.setLogin}
                          getLogin = {this.getLogin()}
                          getHome={this.getHome}
                    />
          }
         else if(!this.state.isConnected && !this.state.inscrit){
          page= <Signup  sInscrire={this.sInscrire}
                         getConnected={this.getConnected}
                         getInscrit={this.getInscrit}
                         login= {this.state.login}
                         setLogin={this.setLogin}
                         getLogin = {this.getLogin()}
                        /> 
        }
     
        else if(this.state.isConnected){
          page = <Accueil getProfile={this.getProfile}
                          getCreateEvent ={this.getCreateEvent}               
                          getExercises ={this.getExercises}
                          setLogout={this.setLogout}
                          getInscrit={this.getInscrit} 
                          page= {this.state.page}
                          cle={this.state.cle}
                          getAccueil={this.getAccueil}
                          getFriends={this.getFriends}
                          getHome={this.getHome}
                          getLogin = {this.getLogin}  
                          setKey={this.setKey}
                          login= {this.state.login}
                          setLogin={this.setLogin}
                          getUserProfile={this.getUserProfile}
                          userP={this.state.userP}
                        />
       }
  
        return (
              <div className="MainPage"> {page}   </div>
          );
      }
    }

export default MainPage;

