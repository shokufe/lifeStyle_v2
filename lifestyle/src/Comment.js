import React, { Component } from 'react';
import './connexion.css'
import axios from 'axios'
import { UncontrolledCollapse, Button, CardBody, Card } from 'reactstrap';
/**
 * Component pour les commentaire de chaque event
 * @author Shokufeh AHMADI SIMABE -- Nassima GHOUT
 */

class Comment extends Component {
        constructor(props){
            super(props)
            this.handleDeleteComment = this.handleDeleteComment.bind(this)
        }
        handleDeleteComment(){
            var code
            var url= new URLSearchParams();
            url.append('key',this.props.cle)
            url.append('idComment',this.props.idComment)
            axios.get('http://localhost:8080/Life_Style/RemoveComment?'+url)
			.then(response => {
                console.log(response)
                code=response.data["code"];
				if(code === 200){
                    console.log("Remove successful")
                    this.props.getHome()
                    this.forceUpdate()
                    this.setState({content:"  "})

                }
                if(code===-1){
                    console.log("key not exist")
                }
                if(code === 3){
                    alert("not allowed to remove")
                }
				
			})  
        }
        render(){
            return (
        
            <div className="card w-75" style={{backgroundColor:'#45484D' , marginTop:'5px' ,color:'white'}}>
            <div className="card-body">
                <p className="card-text"> {this.props.user_name}:{this.props.content} </p>
                <p className="card-text"> date:{this.props.date} </p>
                <Button onClick={()=> this.handleDeleteComment()} color="#7770B2" class="btn btn-outline-success"  >Delete</Button>
            </div>
            </div>
            )
        
                
        }
        }
        export default Comment;
        
        
                