import React, { Component } from 'react';
import Comment from './Comment'
import Parts from './img/participant.png'
import './css/my_style.css';
import comments from './img/comments.png'
import { UncontrolledCollapse, Button, CardBody, Card } from 'reactstrap';
import 'react-notifications-component/dist/theme.css'
import { store } from 'react-notifications-component';
import 'animate.css';
/**
 * Composant évènement qui décrit un évènement qu'on peut rejoindre et/ou commenter
 * @author Shokufeh AHMADI SIMABE -- Nassima GHOUT
 */
import axios from 'axios'
import './connexion.css'
class Event extends Component {
        constructor(props){
            super(props)
            this.state={nbParticipants:"0", content: null , user:null, commentslist:[] ,
            numPart: Parts , commentPIC:comments, com:"0", showSport:true, showEdu:true,showRe:true,
             showBeb:true,showother:true}
            this.handleDeleteEvent = this.handleDeleteEvent.bind(this)
            this.handleJoinEvent = this.handleJoinEvent.bind(this) 
            this.handleComment= this.handleComment.bind(this)
            this.Participant();
            this.Participant = this.Participant.bind(this) 
            this.handleInputComment=this.handleInputComment.bind(this)
            this.getAllComments();
            this.getAllComments =this.getAllComments.bind(this);
            this.Checkfilter();
            this.Checkfilter=this.Checkfilter.bind(this);

        }
Checkfilter(){
  console.log("sporttt",this.props.isCheckedSport)
  console.log("relaxxxx",this.props.isCheckedRe)
  console.log("Eduuuu",this.props.isCheckedEdu)
  console.log("Babbbb",this.props.isCheckedBab)
  console.log("Otherrrrr",this.props.isCheckedOther)
  //dont show sport
  if((this.props.category === "Sport")&&(!this.props.isCheckedSport)){
    this.setState({showSport: false});
  }
  if((this.props.category === "Edu")&&(!this.props.isCheckedEdu)){
    this.setState({showEdu: false});
  }
  if((this.props.category === "Enter")&&(!this.props.isCheckedEnter)){
    this.setState({showEnter: false});
  }
  if((this.props.category === "Bab")&&(!this.props.isCheckedBab)){
    this.setState({showBeb: false});
  }
  if((this.props.category === "relax")&&(!this.props.isCheckedRe)){
    this.setState({showRe: false});
  }
  if((this.props.category === "Other")&&(!this.props.isCheckedOther)){
    this.setState({showother: false});
  }  
}        
/**
 * récupérer le nombre de personnes participants à l'évènement
 */        
Participant(){
    var code
    var url= new URLSearchParams();
    url.append('idEvent',this.props.event_id)
    axios.get('http://localhost:8080/Life_Style/CountPartEvents?'+url)
			.then(response => {
                code=response.data["code"];
				if(code === 200){
                    this.setState({nbParticipants:response.data['nbParticipants']})
                }
				else if(code === -10){
					alert("User not connected false in cle" , this.props.cle)
				}
				else if(code === 1){
                   // alert("User not connected")
                    store.addNotification({
                        title: "Error!",
                        message: "You are not connected",
                        type: 'danger',
                        insert: "top",
                        container: "top-right",
                        animationIn: ["animated", "fadeIn"],
                        animationOut: ["animated", "fadeOut"],
                        dismiss: {
                          duration: 3000,
                          onScreen: true
                        }
                      });
				}
			})  
} 
/**
 * suppression d'un évènement
 * 
 */        
handleDeleteEvent() {
    var code
    var url= new URLSearchParams();
    url.append('key', this.props.cle)
    url.append('id',this.props.id)
    axios.get('http://localhost:8080/Life_Style/DeleteEvent?'+url)
			.then(response => {
                code=response.data["code"];
				if(code === 200){
                    store.addNotification({
                        title: "Successful!",
                        message: "Delete Done.",
                        type: 'success',
                        insert: "top",
                        container: "top-right",
                        animationIn: ["animated", "fadeIn"],
                        animationOut: ["animated", "fadeOut"],
                        dismiss: {
                          duration: 3000,
                          onScreen: true
                        }
                      });
                      this.props.getHome()
                      this.setState({content:"  "})
                }
				else if(code === -1){
                    store.addNotification({
                        title: "Error!",
                        message: "try later",
                        type: 'danger',
                        insert: "top",
                        container: "top-right",
                        animationIn: ["animated", "fadeIn"],
                        animationOut: ["animated", "fadeOut"],
                        dismiss: {
                          duration: 3000,
                          onScreen: true
                        }
                      });
                    
				}
				else if(code === -10){
                    store.addNotification({
                        title: "Error!",
                        message: "try again.",
                        type: 'danger',
                        insert: "top",
                        container: "top-right",
                        animationIn: ["animated", "fadeIn"],
                        animationOut: ["animated", "fadeOut"],
                        dismiss: {
                          duration: 3000,
                          onScreen: true
                        }
                      });
				}
			})   
}   
/**
 * appel du servlet pour rejoindre un évènement
 */
handleJoinEvent(){
    var code
    var url= new URLSearchParams(); 
    url.append('name',this.props.name)
    url.append('owner',this.props.owner)
    url.append('key', this.props.cle)
    axios.get('http://localhost:8080/Life_Style/JoinEvent?'+url)
			.then(response => {
                code=response.data["code"];
				if(code === 200){
                    store.addNotification({
                        title: "Successful!",
                        message: "you joined to this Event.",
                        type: 'success',
                        insert: "top",
                        container: "top-right",
                        animationIn: ["animated", "fadeIn"],
                        animationOut: ["animated", "fadeOut"],
                        dismiss: {
                          duration: 1000,
                          onScreen: true
                        }
                      });
                      this.Participant()
                      this.props.getHome()
                      this.setState({content:"  "})
                }
				else if(code === -10){
                  //  alert("User not connected false in cle" , this.props.cle)
                    store.addNotification({
                        title: "Error!",
                        message: "You are not conntected.",
                        type: 'danger',
                        insert: "top",
                        container: "top-right",
                        animationIn: ["animated", "fadeIn"],
                        animationOut: ["animated", "fadeOut"],
                        dismiss: {
                          duration: 1000,
                          onScreen: true
                        }
                      });
                    
				}
				else if(code === 1){
                   // alert("Already joint")
                    store.addNotification({
                        title: "Warning!",
                        message: "You are already joined.",
                        type: 'warning',
                        insert: "top",
                        container: "top-right",
                        animationIn: ["animated", "fadeIn"],
                        animationOut: ["animated", "fadeOut"],
                        dismiss: {
                          duration: 1000,
                          onScreen: true
                        }
                      });
     
            }
        else{
          store.addNotification({
            title: "Error!",
            message: "there is a probleme to join.",
            type: 'danger',
            insert: "top",
            container: "top-right",
            animationIn: ["animated", "fadeIn"],
            animationOut: ["animated", "fadeOut"],
            dismiss: {
              duration: 1000,
              onScreen: true
            }
          });
        }
			})
} 
/**
 * Commenter un évènement
 */

handleComment(){
    var code
    var url= new URLSearchParams(); 
    url.append('key', this.props.cle)
    url.append('idEvent',this.props.event_id)
    url.append('content', this.state.content)
    axios.get('http://localhost:8080/Life_Style/NewComment?'+url)
			.then(response => {
               // console.log(response.data)
                code=response.data["code"];
                //alert(code)
				if(code === 200){
                   // alert("Comment successful")
                    this.getAllComments()
                    this.setState({content:"  "})
                    //window.location.reload(false);
                    //this.setState(Friend)
                }
				else if(code === -10){
					alert("User not connected false in cle" , this.props.cle)
				}
				else if(code === 1){
					alert("User not connected")
				}
			})
} 

handleInputComment= (e) =>{
    this.setState({content : e.target.value})
  }

getAllComments(){
    var url= new URLSearchParams(); 
    url.append('idEvent',this.props.event_id)
    axios.get('http://localhost:8080/Life_Style/ListComments?'+url)
    .then(response => {

        if(response.data.length>0){
            this.setState({commentslist: Object.values(response.data)})
        }
    })
    
}

render(){
 // const togglecheck1 = (this.props.category === "Sport")&&(this.props.isCheckedSport) ? 'hidden-check1' : 'hidden-check2';
    var l = this.state.commentslist
    const map7 = l.map(item =>  <Comment
                                    idComment={item["idComment"]}
                                    idEvent ={item["idEvent"]}
                                    user_name ={item["user_name"]}
                                    date={item["date"]} 
                                    content={item["content"]}  
                                    cle={this.props.cle}
                                    getHome={this.props.getHome}
                                     
                                />
                         ); 
 
    const shows=this.state.showSport;
    return (
      
        <div className=" mb-2 testborder2"> 
            <div  className="adjustMargin" style={{fontFamily:'\"Times New Roman\", Times, serif'}} >
                 <div className="card-body" >
                   <span className="card-title" style={{color:'#EEF5F8'}}> __{this.props.name}__ </span>
                   <span style={{width:'30%', marginRight:"0px",marginLeft:"auto",fontSize:"1rem"}}>By: {this.props.owner}</span>
               
                    <p className="card-text"> {this.state.nbParticipants} person joined to this event . place : {this.props.lieu}
                    and date is {this.props.date}. Category:{this.props.category} </p>
                    <Button className="join" onClick={()=> this.handleJoinEvent()} class="btn btn-outline-success">Join</Button>
                    <Button className="comment" id={"com"+this.props.event_id} style={{ marginBottom: '0rem' , marginLeft: '1rem' }} class="placeOfBtn">
                    comments
                    </Button>
                    <UncontrolledCollapse toggler={"#com"+this.props.event_id}>
                    <Card>
                    <CardBody>
                      {map7}
                      <textarea id="form7" className="md-textarea form-control" rows="3"  onChange={this.handleInputComment} placeholder="your comment !!!" autocomplete="off" />
                    <Button  onClick={()=> this.handleComment()}  id="toggler" style={{ marginBottom: '1rem',backgroundColor:'#4C070D' ,color:'white'}}>         
                          ok
                    </Button>
                </CardBody>
            </Card>
            </UncontrolledCollapse>
            </div>
        </div>
        </div>
    )
}
}
export default Event;


        