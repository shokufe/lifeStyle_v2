import React, { Component } from 'react';
import './css/bootstrap.min.css';
import './connexion.css';
import './css/my_style.css'
import axios from 'axios'
import 'react-notifications-component/dist/theme.css'
import { store } from 'react-notifications-component';
import 'animate.css';
/**
 * Component de page de login
 * @author Shokufeh AHMADI SIMABE -- Nassima GHOUT
 */
class Login extends Component {
    constructor(props){
      super(props)
      this.state={ password:null, key_bis:null}
      this.handleClick = this.handleClick.bind(this)
      this.InscriptionClick = this.InscriptionClick.bind(this)
      this.updateInputLogin = this.updateInputLogin.bind(this)
      this.updateInputValuePwd = this.updateInputValuePwd.bind(this)
      this.setKey=this.setKey.bind(this)
     // this.ForgotPassword =this.ForgotPassword(this)
    }
    
    setKey(value){
      this.props.setKey(value)
    }
    handleCheckConnection(){
      
    }
    handleClick(){      
      var code
      var url= new URLSearchParams();
      if(this.props.login!==null && this.state.password!==null){
                 
      url.append('login',this.props.login)
      url.append('password',this.state.password)
      axios.get('http://localhost:8080/Life_Style/Connexion?'+url)
        .then(response => {
            code=response.data["code"];
            this.setKey(response.data["key"])
            this.setState({key_bis: response.data["key"]})
          if(code === 200){
            //this.props.getProfile()
           
            this.props.getConnected()
            this.props.getInscrit()
            this.props.getHome()
          }
          else if(code === -1){
            //alert("Pas d'arguments")
            store.addNotification({
              title: "Missing argument!",
              message: "fill all arguments",
              type: 'danger',
              insert: "top",
              container: "top-right",
              animationIn: ["animated", "fadeIn"],
              animationOut: ["animated", "fadeOut"],
              dismiss: {
                duration: 3000,
                onScreen: true
              }
            });
          }
          else if(code === 1){
            //alert("User not exists")
            store.addNotification({
              title: "User is not exists!",
              message: "check your username or pass and try again",
              type: 'danger',
              insert: "top",
              container: "top-right",
              animationIn: ["animated", "fadeIn"],
              animationOut: ["animated", "fadeOut"],
              dismiss: {
                duration: 3000,
                onScreen: true
              }
            });
          }
          else if(code === 2){
           // alert("Wrong password")
            store.addNotification({
              title: "Wrong password!",
              message: "check your pass and try again",
              type: 'danger',
              insert: "top",
              container: "top-right",
              animationIn: ["animated", "fadeIn"],
              animationOut: ["animated", "fadeOut"],
              dismiss: {
                duration: 3000,
                onScreen: true
              }
            });
          
          }
          /*else if(code === 3){
           // this.props.getKey(this.state.key_bis)
            this.props.getConnected()
            this.props.getHome()
           
            */
            //alert("utilisateur deja connecté")
         // }
          else{
           // alert("TEST")
          }
        })   
      }
      else{
        store.addNotification({
          title: "Error!",
          message: "fill in the blanks",
          type: 'error',
          insert: "top",
          container: "top-right",
          animationIn: ["animated", "fadeIn"],
          animationOut: ["animated", "fadeOut"],
          dismiss: {
            duration: 3000,
            onScreen: true
          }
        });
 
      }
    }
      InscriptionClick(){
        this.props.sInscrire()
      } 
      ForgotPassword(){
        this.props.fpsw()
      }
      ProfileClick(){
       
      } 
      updateInputLogin = event => {
        
        var tmp = event.target.value      
       this.props.setLogin(tmp)            
      }
      updateInputValuePwd = event =>{
        this.setState ({password : event.target.value})
      }
    render() {
        return(
          <div className="Connexion loginImg" >
            <div className="container">
              <div className="row">
                <div className="col-md-5 mx-auto">
                  <div id="first">
                    <div className="myform form ">
                      <div className="logo mb-3">
                        <div className="col-md-12 text-center">
                          <h1>Login</h1>
                        </div>
                     </div>
                     <div forname="login">
                        <div className="form-group">
                          <label htmlFor="exampleInputEmail1">Login</label>
                          <input type="email" name="email" onChange={this.updateInputLogin} className="form-control" id="email" aria-describedby="emailHelp" placeholder="Login"/>
                        </div>
                      <div className="form-group">
                         <label htmlFor="exampleInputEmail1">Password</label>
                         <input type="password" name="password" id="password" onChange={this.updateInputValuePwd} className="form-control" aria-describedby="emailHelp" placeholder="Enter Password"/>
                      </div>
                     
                      <div className="col-md-12 text-center ">
                        <button type="submit"  onClick={()=>this.handleClick()} className=" btn btn-block mybtn btn-primary tx-tfm">Login</button>
                      </div>
                      <div className="col-md-12 ">
                        <div className="login-or">
                          <hr className="hr-or"/>
                          <span className="span-or">or</span>
                        </div>
                      </div>
                     <div className="form-group">
                       <p className="text-center">Don't have account? <a href="#" onClick={()=>this.InscriptionClick()} id="signup">Sign up here</a></p>
                       <p className="text-center"><a href="#" onClick={()=>this.ForgotPassword()} id="signup">Forgot Password?</a></p>
                     </div>
                 </div>
            </div>
            </div>
            </div>
            </div>
            </div>
    
      </div>     
  
        )}
  }
  
  export default Login;