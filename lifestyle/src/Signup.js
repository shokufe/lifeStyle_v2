import React, { Component } from 'react';
import './css/bootstrap.min.css'
import './css/my_style.css'
import axios from 'axios'
import recaptcha from 'react-recaptcha'
import 'react-notifications-component/dist/theme.css'
import { store } from 'react-notifications-component';
import 'animate.css';

/**
 * component de page de Signup
 * 
 * @author Shokufeh AHMADI SIMABE -- Nassima GHOUT
 */

class Signup extends Component {
    constructor(props) {
        super(props)
        this.state = { password: null, password2: null, nom: null, prenom: null, sexe: null, age: null,dateDeN: null, mail: null, 
            phone: null, poids: null, taille: null, adresse: null, ZipCode: null, country:"fr", msg: "" }
        this.updateInputLogin = this.updateInputLogin.bind(this)
        this.updateInputPassword = this.updateInputPassword.bind(this)
        this.updateInputNom = this.updateInputNom.bind(this)
        this.updateInputPrenom = this.updateInputPrenom.bind(this)
        this.updateInputSexe = this.updateInputSexe.bind(this)
        this.updateInputAge = this.updateInputAge.bind(this)
        this.updateInputMail = this.updateInputMail.bind(this)
        this.updateInputPhone = this.updateInputPhone.bind(this)
        this.updateInputPoids = this.updateInputPoids.bind(this)
        this.updateInputTaille = this.updateInputTaille.bind(this)
        this.updateInputAdresse = this.updateInputAdresse.bind(this)
        this.updateInputZipCode = this.updateInputZipCode.bind(this)
        this.handleClick = this.handleClick.bind(this)
        this.confirmPwd = this.confirmPwd.bind(this)
        this.updateInputCountry = this.updateInputCountry.bind(this)
        this.handleClicklogin =this.handleClicklogin.bind(this)
        
    }
    handleClicklogin(){
       window.open('Login.js',"_self")
    }
    handleClick() {

        if (this.props.login !== null && this.state.phone !== null && this.state.nom !== null && 
            this.state.prenom !== null && this.state.password !== null && this.state.ZipCode !== null) {
            var code
            var url = new URLSearchParams();
            url.append('login', this.props.login)
            url.append('password', this.state.password)
            url.append('password2', this.state.password2)
            url.append('nom', this.state.nom)
            url.append('prenom', this.state.prenom)
            url.append('sexe', this.state.sexe)
            url.append('dateDeN', this.state.dateDeN)
            url.append('mail', this.state.mail)
            url.append('phone', this.state.phone)
            url.append('poids', this.state.poids)
            url.append('taille', this.state.taille)
            url.append('adresse', this.state.adresse)
            url.append('ZipCode', this.state.ZipCode)
            url.append('country', this.state.country)
            axios.get('http://localhost:8080/Life_Style/CreateUser?' + url)
                .then(response => {
                    console.log("i'm in sign up", response.data);
                    code = response.data["code"]

                    if (code === 200) {
                        this.setState({ msg: "OK" })
                        this.props.getInscrit()
                        store.addNotification({
                            title: "Inscription Successful!",
                            message: "enjoy!",
                            type: 'success',
                            insert: "top",
                            container: "top-right",
                            animationIn: ["animated", "fadeIn"],
                            animationOut: ["animated", "fadeOut"],
                            dismiss: {
                              duration: 3000,
                              onScreen: true
                            }
                          });
                    }
                    else if (code === -1) {
                        this.setState({ msg: "Pas d'arguments" })
                    }
                    else if (code === -11) {
                        this.setState({ msg: "two password are not same" })
                    }
                    else if (code === 1) {
                        this.setState({ msg: "Login exist deja" })
                    }
                    else if (code === 2) {
                        this.setState({ msg: "Wrong password" })
                    }
                    else if (code === 12) {
                        this.setState({ msg: "this email has an account" })
                    }
                    else if (code === -2){
                        this.setState({msg: "Format données incorrect (poids, taille, age)"})
                        //alert ("Format données incorrect (poids, taille, age)")
                    }
                })
        } else {
            //alert("champs obligatoire")
            store.addNotification({
                title: "fill all blanks",
                message: "b",
                type: 'danger',
                insert: "top",
                container: "top-right",
                animationIn: ["animated", "fadeIn"],
                animationOut: ["animated", "fadeOut"],
                dismiss: {
                  duration: 3000,
                  onScreen: true
                }
              });
        }
       /* store.addNotification({
            title: this.state.msg,
            message: "enjoy!",
            type: 'danger',
            insert: "top",
            container: "top-right",
            animationIn: ["animated", "fadeIn"],
            animationOut: ["animated", "fadeOut"],
            dismiss: {
              duration: 3000,
              onScreen: true
            }
          });*/
      //  alert(this.state.msg);
    }

    updateInputLogin = event => {
        var tmp = event.target.value
        this.props.setLogin(tmp)
    }
    updateInputNom = event => {
        this.setState({ nom: event.target.value })
    }
    updateInputPrenom = event => {
        this.setState({ prenom: event.target.value })
    }
    updateInputMail = event => {
        this.setState({ mail: event.target.value })
    }
    updateInputPassword = event => {
        this.setState({ password: event.target.value })
    }
    confirmPwd = event => {
        this.setState({ password2: event.target.value })
    }
    updateInputPhone = event => {
        this.setState({ phone: event.target.value })
    }
    updateInputSexe = event => {
        this.setState({ sexe: event.target.value })
    }
    updateInputAge = event => {
        this.setState({ dateDeN: event.target.value })
    }
    updateInputPoids = event => {
        this.setState({ poids: event.target.value })
    }
    updateInputTaille = event => {
        this.setState({ taille: event.target.value })
    }
    updateInputAdresse = event => {
        this.setState({ adresse: event.target.value })
    }
    updateInputZipCode = event => {
        this.setState({ ZipCode: event.target.value })
    }
    updateInputCountry = event => {
        this.setState({ Country:event.target.Value})
    }
    render() {

        return (

  <div className="Connexion signup" >
            <div className="container">
              <div className="row">
                <div className="col-md-5 mx-auto">
                  <div id="first">
                    <div className="myform form ">
                      <div className="logo mb-3">
                        <div className="col-md-12 text-center">
                          <h1>Signup</h1>
                        </div>
                     </div>
                     <div forname="login">
                        <div className="form-group">
                            <input type="text" onChange={this.updateInputLogin} className="form-control" placeholder="Login *" />
                        </div>
                        <div className="form-row">
                            <div className="form-group col">
                                <input type="text" onChange={this.updateInputNom} className="form-control" placeholder="First Name *" />
                            </div>
                            < div className="form-group col">
                                <input type="text" className="form-control" onChange={this.updateInputPrenom} placeholder="Last Name *" />
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="form-group col">
                                <input type="password" className="form-control" onChange={this.updateInputPassword} placeholder="Password *" />
                            </div>
                            <div className="form-group col">
                                <input type="password" className="form-control" onChange={this.confirmPwd} placeholder="Confrm Password *" />
                            </div>
                        </div>
                        <div className="form-group">
                            <input type="email" className="form-control" onChange={this.updateInputMail} placeholder="Your Email *" />
                        </div>
                        <div className="form-row">
                            <div className="form-group col">
                                <input type="poids" id="poids" name="text" className="form-control" onChange={this.updateInputPoids} placeholder="Your weights *" />
                            </div>
                            <div className="form-group col">
                                <input type="taille" id="taille" name="text" className="form-control" onChange={this.updateInputTaille} placeholder="Your tall *" />
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="form-group col">
                                <select type="sexe" id="sexe" name="text" className="form-control"   value={this.state.selectValue} onChange={this.updateInputSexe} placeholder="sexe *">
                                    <option value="null">choose one gender </option>
                                    <option value="F">Woman</option>
                                    <option value="M">Man</option>
                                </select>
                            </div>
                          
                            <div className="form-group col ">
                                <div className="col-sm-10 col">
                                    <input type="date" id="age" name="Date-Of-Birth" max={this.state.today} className="form-control"  onChange={this.updateInputAge} placeholder="Date-Of-Birth"/>
                                </div>
                              </div>
                        </div>
                       <div className="form-group">
                            <input type="adresse" id="adresse" name="text" className="form-control" onChange={this.updateInputAdresse} placeholder="Your adresse *" />
                        </div>
                        <div className="form-group">
                            <input type="tel" id="phone" name="text" className="form-control" onChange={this.updateInputPhone} placeholder="Your phone number *" />
                        </div>
                        <div className="form-row">
                            <div className="form-group col">
                                <input type="ZipCode" id="ZipCode" name="text" className="form-control" onChange={this.updateInputZipCode} placeholder="Your postal code *" />
                            </div>       
                            <div className="form-group col">
                                <select type="country" id="country" name="text" className="form-control"   value={this.state.selectValue} onChange={this.updateInputCountry} placeholder="Country code* (ex: Fr pour France) *">
                                    <option value="null">choisit entre les deux </option>
                                    <option value="fr">France</option>
                                    <option value="us">USA</option>
                                    <option value="alg">Algérie</option>
                                    <option value="ir">Iran</option>
                                </select>
                        </div> 
                        </div> 
                       
                        <recaptcha></recaptcha>
                      <div className="col-md-12 text-center ">
                        <button type="submit"  onClick={()=>this.handleClick()} className=" btn btn-block mybtn btn-primary tx-tfm">Signup</button>
                        <button type="submit"  onClick={()=>this.handleClicklogin()} className=" btn btn-block mybtn btn-primary tx-tfm">login</button>
                      </div>
                 </div>
            </div>
            </div>
            </div>
            </div>
            </div>
    
      </div>     

        )

    }
}

export default Signup;
