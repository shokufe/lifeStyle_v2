import React from 'react';
import logo from './logo.svg';
import './App.css';
import ReactNotification from 'react-notifications-component'

function App() {
  return (
    <div >
      <ReactNotification />
      <header className="App-header">
      </header>
    </div>
  );
}

export default App;
