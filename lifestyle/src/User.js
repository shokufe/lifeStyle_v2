import React, { Component } from 'react';
import './css/my_style.css';
import axios from 'axios'
import './connexion.css'
import UserProfile from './UserProfile'
import a from './img/avatar/sh.jpg'
import 'react-notifications-component/dist/theme.css'
import { store } from 'react-notifications-component';
import 'animate.css';
/**
 * Component qui represent tous les users qui sont pas amie
 * 
 * @author Shokufeh AHMADI SIMABE -- Nassima GHOUT -- V1
 * @author Shokoufeh AHMADI SIMAB -- V2
 */

class User extends Component {
    constructor(props){
        super(props)
        this.state={avatar:a, userprofile:false}
        this.followUser =this.followUser.bind(this)
        this.goUserProfile=this.goUserProfile.bind(this)
    }
    /**
     * ajout un user dans le liste d'amie
     */
goUserProfile(){
  this.setState({userprofile: true , userprofile_id:this.props.user_id})
  this.props.getUserProfile(this.props.user_id);
}
getUserProfilee(){
  var code
  var url= new URLSearchParams();
  
  //alert("UserProfile11 "+this.props.user_id)
  url.append('login', this.props.user_id)
  url.append('key',this.props.cle)
  axios.get('http://localhost:8080/Life_Style/GetUserProfile?'+url)
    .then(response => {
              code=response.data["code"];
      if(code === 200){
                  this.setState({user_id:response.data['user_id'],nom: response.data['nom'],prenom: response.data['prenom'],sexe: response.data['sexe'],
                      mail: response.data['mail'],phoneNumber: response.data['phoneNumber']
                          ,adress:response.data['adress'] , country : response.data['Country'], age:response.data['age'] })
        
              }
              else{
                    alert(code)
              }
  })
}
followUser(){

    var code
    var url= new URLSearchParams();
    url.append('key', this.props.cle)
    url.append('id_friend',this.props.user_id)
    axios.get('http://localhost:8080/Life_Style/AddFriend?'+url)
			.then(response => {
                code=response.data["code"];
				if(code === 200){
            // alert("Add Successful")
             store.addNotification({
              title: "Add successful!",
              message: "You have new friend now",
              type: 'success',
              insert: "top",
              container: "top-right",
              animationIn: ["animated", "fadeIn"],
              animationOut: ["animated", "fadeOut"],
              dismiss: {
                duration: 5000,
                onScreen: true
              }
            });
            this.props.getAllUsers()
            this.setState({content:"  "})
           // window.open('Home.js',"_self")
                }
				else if(code === 2){
         // alert("Add Successful")
          store.addNotification({
           title: "Not allowed!",
           message: "You are already friends",
           type: 'warning',
           insert: "top",
           container: "top-right",
           animationIn: ["animated", "fadeIn"],
           animationOut: ["animated", "fadeOut"],
           dismiss: {
             duration: 3000,
             onScreen: true
           }
         });
				}
				else if(code === 1000){
          //alert("User not Exist")
          store.addNotification({
            title: "Not allowed!",
            message: "this user not exists anymore ",
            type: 'warning',
            insert: "top",
            container: "top-right",
            animationIn: ["animated", "fadeIn"],
            animationOut: ["animated", "fadeOut"],
            dismiss: {
              duration: 3000,
              onScreen: true
            }
          });
				}
			})   
}
render(){
  var userp;
  if(this.state.userprofile){
    userp = <UserProfile 
                      cle={this.props.cle}
                      login= {this.props.login}
                      getUserProfilee={this.getUserProfilee}
                    />
  }
return(
 
    
    <div className="card showUser">
      <div className="card-body ">
        <h6 className="card-title">
        <button type="button" className="btn btn-outline-success" onClick={()=> this.goUserProfile()}>{this.props.login} </button>
        </h6>
        <button type="button" className="btn btn-outline-success" onClick={()=> this.followUser()} >Follow</button>
      </div>
    </div>
 

)

    
}
}
export default User;


    