import React, { Component } from 'react';
import './connexion.css'

/**
 *Ce component représente chaque Exercice
 * @author Shokufeh AHMADI SIMABE -- Nassima GHOUT
 */
class Activity extends Component {
        constructor(props){
            super(props)
        }
    
render(){
    return (

    <div className="card w-75" style={{backgroundColor:"#4C1A1E",opacity:'0.9',marginBottom:'23px',color:'white'}}>
    <div className="card-body" style={{opacity:'0.9'}}>
        <h1>{this.props.name}</h1>
        <p className="card-text">description : {this.props.desc} </p>
    </div>
    </div>
    )

        
}
}
export default Activity;


        