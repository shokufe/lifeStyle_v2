import React, { Component } from 'react';
import './css/bootstrap.min.css'
import Friend from './Friend'
import axios from 'axios'
import 'react-notifications-component/dist/theme.css'
import { store } from 'react-notifications-component';
import 'animate.css';
import { UncontrolledCollapse, Button, CardBody, Card ,Container} from 'reactstrap';
/**
 * Component de la page d' ami
 * @author Shokufeh AHMADI SIMABE -- Nassima GHOUT --V1
 * @author Shokufeh AHMADI SIMABE --V2
 */
class Friends extends Component {
    constructor(props) {
        super(props)
        this.state={listFriend:[], avatar:'./img/avatar/sh.jpg'}
        this.AllFriends()
        this.AllFriends =this.AllFriends.bind(this)
    }
    AllFriends(){
        var url= new URLSearchParams();
        url.append('key',this.props.cle)
		axios.get('http://localhost:8080/Life_Style/ListFriends?'+url)
			.then(response => {
                console.log("response ", response.data)
                if(response.data.length >0)
                {
                
                        this.setState({listFriend: Object.values(response.data)})

                        //console.log (this.state.listFriend)
                        //console.log ("length  ",this.state.listFriend.length)
                }
                else {
                    //alert("there is no friends")
                    store.addNotification({
                        title: "No Friends!",
                        message: "add some friends",
                        type: 'warning',
                        insert: "top",
                        container: "top-right",
                        animationIn: ["animated", "fadeIn"],
                        animationOut: ["animated", "fadeOut"],
                        dismiss: {
                          duration: 3000,
                          onScreen: true
                        }
                      });
                }

			})  
    }
    render() {
        
        var l = this.state.listFriend 
        const map4 = l.map(item =>  <Friend
                            id={item["id_friend"]}
                            date={item["date"]}  
                            login= {item["login"]}
                            cle = {this.props.cle}      
                            AllFriends={this.AllFriends} 
                            getUserProfile={this.props.getUserProfile}                     
                                    />
                            ); 
        return(       
            <Container className="friendImg" style={{marginRight:'0',marginLeft:'0',marginRight:'0',marginLeft:'0',paddingRight:'0',paddingLeft:'0', maxWidth: '1450px'}} >
                <div class="parallax-bg3"></div>
                {map4}
            </Container>
        )
     
            
    }

}
export default Friends;
