import React, { Component } from 'react';
import axios from 'axios'
import './connexion.css'
import UserImg from './img/avatar/usr.png'
import {Button} from "reactstrap";
import 'react-notifications-component/dist/theme.css'
import { store } from 'react-notifications-component';
import 'animate.css';
import { UncontrolledCollapse, CardBody, Card ,Container} from 'reactstrap';
/**
 * Composant représentant un ami
 * 
 * @author Shokufeh AHMADI SIMABE -- Nassima GHOUT
 */
class Friend extends Component {
        constructor(props){
            super(props)
            this.state={avatar:UserImg}
            this.handlUnFriend = this.handleUnFriend.bind(this)
            this.handleViewUserProfile=this.handleViewUserProfile.bind(this)
        }
    
        /**
         * gestion de 
         */
handleUnFriend(){
    var code
    var url= new URLSearchParams();
    url.append('key', this.props.cle)
    url.append('id_friend',this.props.id)
    axios.get('http://localhost:8080/Life_Style/RemoveFriend?'+url)
			.then(response => {
				code=response.data["code"];
				if(code === 200){
          store.addNotification({
            title: "Successful!",
            message: "you remove this friends successfully",
            type: 'success',
            insert: "top",
            container: "top-right",
            animationIn: ["animated", "fadeIn"],
            animationOut: ["animated", "fadeOut"],
            dismiss: {
              duration: 3000,
              onScreen: true
            }
          });
          this.props.AllFriends()
          this.setState({content:"  "})
         //window.open("Friends.js","_self")
                }
				else if(code === -1){
          //alert("Pas d'arguments")
          store.addNotification({
            title: "Error!",
            message: "Not possible to remove your friend now",
            type: 'danger',
            insert: "top",
            container: "top-right",
            animationIn: ["animated", "fadeIn"],
            animationOut: ["animated", "fadeOut"],
            dismiss: {
              duration: 3000,
              onScreen: true
            }
          });
				}
				else if(code === 1){
          //alert("User not connected")
          store.addNotification({
            title: "Error!",
            message: "you are not connected",
            type: 'danger',
            insert: "top",
            container: "top-right",
            animationIn: ["animated", "fadeIn"],
            animationOut: ["animated", "fadeOut"],
            dismiss: {
              duration: 3000,
              onScreen: true
            }
          });
				}
			})   
}    
handleImg(){
    this.setState({avatar : 'img/avatar/sh.jpg'})
}  
handleViewUserProfile(){
  alert(this.props.id)
  this.props.getUserProfile(this.props.id);
}  
render(){
    return (
<Card className="order-xl-2 mb-5 mb-xl-0 sizeBox" >
    <div class="row no-gutters">
      <div class="col-md-4">
        <img src={this.state.avatar} class="card-img" alt="..." />
      </div>
      <div class="col-md-8">
        <div class="card-body">
          <h5 class="card-title" style={{fontSize:"bold" }} onClick={() => this.handleViewUserProfile()} > {this.props.login}</h5>
          <p class="card-text">My name is {this.props.name} we are friends since {this.props.date} </p>

          <Button   onClick={() => this.handleUnFriend()}   color="primary" id="toggler" style={{ marginBottom: '1rem' }}> UnFriend</Button>

        </div>
      </div>
    </div>
</Card>
   )
    
    
    }
    }
    export default Friend;
    