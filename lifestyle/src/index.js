import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import MainPage from './MainPage';
import ReactNotification from 'react-notifications-component'
/**
 * 
 * @author Shokufeh AHMADI SIMABE -- Nassima GHOUT
 */

ReactDOM.render(
 
  <React.StrictMode>
    <ReactNotification />
    <MainPage />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
