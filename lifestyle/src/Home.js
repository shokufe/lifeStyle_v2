  
import React, { Component } from 'react';
import axios from 'axios'
import Event from './Event'
import User from './User'
import './connexion.css'
import './css/my_style.css'
import {Container, Card } from "reactstrap";
import 'react-notifications-component/dist/theme.css'
import { store } from 'react-notifications-component';
import 'animate.css';

/**
 * Composant de la page Home on y affiche les utilisateurs qui sont pas nos amis avec possiblité de les follow
 * et la liste des évènements existant
 * 
 * @author Shokoufeh AHMADI SIMABE -- Nassima GHOUT --V1
 * @author Shokoufeh AHMADI SIMAB -- V2
 */

class Home extends Component {
        constructor(props){
            super(props)
            this.state={listM:[], message: null , listAllEvents: [] ,listUsers: [],isCheckedEdu: true,
              isCheckedEnter: true,isCheckedBab: true,isCheckedother: true,isCheckedRe: true,isCheckedSport: true,call:false}
            this.allEvents()
            this.allEvents =this.allEvents.bind(this)
            this.getAllUsers()
            this.getAllUsers=this.getAllUsers.bind(this)
           // this.handleFilter=this.handleFilter.bind(this)
        }
        /**
         * Appel de la servlet pour récupérer tout les évènements
         */
        allEvents(){
          axios.get('http://localhost:8080/Life_Style/ListAllEvents')
          .then(response => {
                if(response.data.length>=0){
                     this.setState({listAllEvents: Object.values(response.data)})
                }
                else {
                  store.addNotification({
                    title: "Error!",
                    message: "Cannot Show all Events now",
                    type: 'error',
                    insert: "top",
                    container: "top-right",
                    animationIn: ["animated", "fadeIn"],
                    animationOut: ["animated", "fadeOut"],
                    dismiss: {
                      duration: 3000,
                      onScreen: true
                    }
                  });
                }
            })   
        }
        /**
         * récupérer la liste des utilisateurs
         */
        getAllUsers(){
         var url= new URLSearchParams();
          url.append('key', this.props.cle)
          axios.get('http://localhost:8080/Life_Style/ListUsers?'+url)
          .then(response => {
              if(response.data.length>0){
              this.setState({listUsers: Object.values(response.data)})
         }
		  	})   
        }    
     
    render(){
      var map3;
     
        var l = this.state.listAllEvents
            map3 = l.map(item =>  <Event
                                      user_id={this.props.user_id}
                                      event_id ={item["event_id"]}
                                      owner={item["owner"]}  
                                      name= {item["name"]}
                                      date={item["date"]}
                                      lieu={item["lieu"]}
                                      category={item["category"]}
                                      created_at={item["created_at"]}
                                      cle = {this.props.cle}
                                      getHome ={this.props.getHome}
                                      isCheckedBab={this.state.isCheckedBab}
                                      isCheckedEdu={this.state.isCheckedEdu}
                                      isCheckedRe={this.state.isCheckedRe}
                                      isCheckedEnter={this.state.isCheckedEnter}
                                      isCheckedSport={this.state.isCheckedSport}
                                      isCheckedOther={this.state.isCheckedother}
                                  />
                           ); 
    
      
  var l1 = this.state.listUsers
  const map9 = l1.map(item =>  <User
                                  user_id={item["user_id"]}
                                  login ={item["user_login"]}
                                  cle = {this.props.cle}
                                  listUsers={this.state.listUsers}
                                  // setFriends={this.setFriends}
                                  getAllUsers={this.getAllUsers}
                                  getUserProfile={this.props.getUserProfile}
                                  page={this.props.page}
                               />
                                              );                      
    return(
      <Container className="homeContainer"style={{marginRight:'0',marginLeft:'0',marginRight:'0',marginLeft:'0',paddingRight:'0',paddingLeft:'0', maxWidth: '1450px'}} >
        <Card style={{marginRight:'0',marginLeft:'0',width:'100%'}} className="backim">  
          <div class="parallax-bg2"></div>
            <div className="homeImg"  >
              <div className="row users" >
                {map9}
              </div>
              <div className="row fix_div backImg" style={{marginLeft:'20%'}}>
                 {map3}       
              </div>

            </div>
          </Card>
      </Container>
    );
}}
export default Home;