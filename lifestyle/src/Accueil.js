import React, { Component } from 'react';

import {NavItem} from 'reactstrap';
import axios from 'axios'
import Friends from './Friends';
import Profile from './Profile';
import MyAlerts from './MyAlerts';
import CreateEvent from './CreateEvent';
import Exercises from './Exercises';
import UserProfile from './UserProfile';
import Home from './Home';
import Cloud from './img/meteo/cloud.jpg';
import LightRain from "./img/meteo/RainCloudSun.jpg";
import Rain from "./img/meteo/rain.jpg";
import Snow from "./img/meteo/snow.jpg";
import Eclaire from "./img/meteo/eclair.jpg";
import Sunny from "./img/meteo/suuny.jpg";
import Windy from "./img/meteo/wind.jpg";
import UserSample from "./img/avatar/usr.png"
import './css/my_style.css'
import './css/Accueil.css'
import './js/myJavaScript'
import { Avatar } from 'antd';
import 'react-notifications-component/dist/theme.css'
import { store } from 'react-notifications-component';
import 'animate.css';
import Notifs from './Notifs';
/**
 * Classe représesentant le composant Accueil, qui est le composant principal
 * il permet la transition entre page, au sens où il partage l'ecran en 2
 * une partiepermanente où se trouve les bouttons permettant de changer de page
 * et le contenu qui change selon la page choisi
 * Une fois connecté on est amené à la Page Home
 * 
 * @author Shokufeh AHMADI SIMABE -- Nassima GHOUT --v1
 *
 */


class Accueil extends Component {
    constructor(props){
      super(props)
      this.state={ListUsers:[], listUsers:[], listFriends:[], listNotification:[], existanceNotif:false, user_id :null , loginp:null,userp:null,
        login:null, tempMax:null, tempMin:null , typeWeath :null ,lat: null , compRes:0,
        lng:null,description:null ,imge:Sunny, userimg:UserSample , userIdprofile:null, userLoginprofile:null}
      this.handleCheckDate=this.handleCheckDate.bind(this)
      this.handleCheckDate()
      this.Users=this.Users.bind(this)
      this.getMyFriends=this.getMyFriends.bind(this)
      this.setListFriends=this.setListFriends.bind(this)
      this.setId= this.setId.bind(this)
      this.handleHome = this.handleHome.bind(this)
      this.handleProfile = this.handleProfile.bind(this)
      this.handleUserProfile = this.handleUserProfile.bind(this)
      this.handleFriends = this.handleFriends.bind(this)
      this.handleAccueil = this.handleAccueil.bind(this)
      this.handleCreateEvent = this.handleCreateEvent.bind(this) 
      this.handleExercises = this.handleExercises.bind(this)//here
      this.handleClick= this.handleClick.bind(this)
      this.weather= this.weather.bind(this)
      this.setImg = this.setImg.bind(this);
      this.handleNotify()
      this.handleNotify=this.handleNotify(this);
      this.weather()
      this.getAllUsers=this.getAllUsers.bind(this)
      this.getAllUsers()
      //this.checkUserProfile=this.checkUserProfile.bind(this)

    }
    /**
     * permet de changer la photo de profil
     * (non fonctionnel pour le moment)
     * @param {*} value 
     */
    setImg(value){
      this.setState({img: value });
    }
    getAllUsers(){
      var url= new URLSearchParams();
       url.append('key', this.props.cle)
       axios.get('http://localhost:8080/Life_Style/ListUsers?'+url)
       .then(response => {
           if(response.data.length>0){
           this.setState({listUsers: Object.values(response.data)})
      }
     })   
     }
    /**
     * appeler la servlet weather pour récupérer la météo, et la latitude et langetude selon l'adresse
     * de l'utilisateur, à fin de retrouver les parcs à proximité
     */
    weather(){
      var code
      var url= new URLSearchParams(); 
      url.append('key',this.props.cle)
      axios.get('http://localhost:8080/Life_Style/ShowWeather?'+url)
      .then(response => {
      code=response.data["code"];
      if(code === 200){
                      this.setState({tempMax:response.data['tempMax'] ,tempMin: response.data['tempMin'],typeWeath: response.data['typeWeath'],
                      lat: response.data['lat'],lng: response.data['lng'] , description: response.data['description']})
                  if(this.state.description === "light rain"){
                      this.setState({imge : LightRain})
                  } 
                  if(this.state.description === "rain" || this.state.description === "rainy"){
                      this.setState({imge : LightRain})
                  } 
                  else if(this.state.description === "Sunny" || this.state.description === "sunny" || this.state.description === "sun") 
                  {
                      this.setState({imge : Sunny})
                  }    
                  else if(this.state.description === "snow" || this.state.description==="light snow" || this.state.description==="rain and drizzle") 
                  {
                      this.setState({imge : Snow})
                  }   
                  else if(this.state.description === "Cloud" || this.state.description === "light cloud" || this.state.description==="few clouds" ||this.state.description ==="overcast clouds"||this.state.description==="broken clouds"||this.state.description==="scattered cloud") 
                  {
                      this.setState({imge : Cloud})
                  }   
                  else if(this.state.description === "eclaire") 
                  {
                      this.setState({imge : Eclaire})
                  }   
                  else if(this.state.description === "Windy" || this.state.description === "wind") 
                  {
                      this.setState({imge : Windy})
                  }   
              }
       
      else if(code === 1 ||code===-1){
        store.addNotification({
          title: "Error!",
          message: "Connextion lost",
          type: 'error',
          insert: "top",
          container: "top-right",
          animationIn: ["animated", "fadeIn"],
          animationOut: ["animated", "fadeOut"],
          dismiss: {
            duration: 3000,
            onScreen: true
          }
        });
      }
    })   
  }
  handleNotify(){
    var url= new URLSearchParams();
          url.append('key', this.props.cle)
          axios.get('http://localhost:8080/Life_Style/Notification?'+url)
          .then(response => {
              if(response.data.length>0){
              this.setState({listNotification: Object.values(response.data), compRes: response.data.length})
            
              }
		  	})   
  }
    /**
     * changer l'Id du user
     * @param {*} tmp 
     */
    setId(tmp){
      this.setState({user_id:tmp})
    }
    /**
     * Gestion de Accueil
     */
    handleAccueil(){
        this.props.getAccueil()
    }
    /**
     * Gestion de l'appuie sur le boutton Home
     */
    handleHome(){
      this.props.getHome()
    }
    /**
     * Gestion du boutton Friends
     */
    handleFriends(){
        this.props.getFriends()
    }
    /**
     * Allerr au profil
     * @param {*} value 
     */
    handleProfile(value){
        this.props.getProfile(value)
    }
    handleUserProfile(value){
      this.props.getUserProfile(value)
    }
    handleUsers(){
        this.props.getUsers()
    }
    /**
     * aller à la page évènemnt pour en créer de nouveaux
     */
    handleCreateEvent(){  
        this.props.getCreateEvent()
    }
    /**
     * aller à la page exercices et voir les exercices suggéré du jour via l'api
     */
    handleExercises(){
        this.props.getExercises();
    }

    setListFriends(tmp){
      this.setState({listFriends:tmp})
    }
    /**
     * appel de la servlet CheckDate 
     * permettant de retirer de la liste les évènements qui sont passé (appelé à chaque connexion)
     * 
     */
    handleCheckDate(){
      var code
      var url= new URLSearchParams(); 
      url.append('key',this.props.cle)
       axios.get('http://localhost:8080/Life_Style/CheckDates?'+url)
        .then(response => {
          code=response.data["code"];
          console.log(code)
        })
    }
    /**
     * Gestion de la déconnexion : appel servlet logout
     * 
     */
    handleClick(){
      var code
      var url= new URLSearchParams(); 
      url.append('key',this.props.cle)
      
      axios.get('http://localhost:8080/Life_Style/Logout?'+url)
        .then(response => {
            code=response.data["code"]
          if(code === 200){
            this.props.setLogout()
            this.props.getInscrit()
          }
          else if(code === 10){
            store.addNotification({
              title: "Error!",
              message: "Invalide key",
              type: 'error',
              insert: "top",
              container: "top-right",
              animationIn: ["animated", "fadeIn"],
              animationOut: ["animated", "fadeOut"],
              dismiss: {
                duration: 3000,
                onScreen: true
              }
            });
          }
          
          else if(code === 100){
            store.addNotification({
              title: "Error!",
              message: "Try again",
              type: 'error',
              insert: "top",
              container: "top-right",
              animationIn: ["animated", "fadeIn"],
              animationOut: ["animated", "fadeOut"],
              dismiss: {
                duration: 3000,
                onScreen: true
              }
            });
        }
        })   
        
    }
    /**
     * Appel de la servlet listMyFriends pour récupérer ma ilste d'amis
     */
    getMyFriends(){
      var url= new URLSearchParams();
   
      url.append('login',this.props.login)
      axios.get('http://localhost:8080/Life_Style/ListFriends?'+url)
        .then(response => {
            if(typeof response.data != 'undefined'){
             var code = response.status
              if(code === 200){
              this.setState({listFriends: response.data["login"]})

               }
               else if(code === -1){
                store.addNotification({
                  title: "Error!",
                  message: "security Issue",
                  type: 'error',
                  insert: "top",
                  container: "top-right",
                  animationIn: ["animated", "fadeIn"],
                  animationOut: ["animated", "fadeOut"],
                  dismiss: {
                    duration: 3000,
                    onScreen: true
                  }
                });
              }
                else if(code === 1){
                  store.addNotification({
                    title: "Error!",
                    message: "Security Issue",
                    type: 'error',
                    insert: "top",
                    container: "top-right",
                    animationIn: ["animated", "fadeIn"],
                    animationOut: ["animated", "fadeOut"],
                    dismiss: {
                      duration: 3000,
                      onScreen: true
                    }
                  });
             }
          
        }
        else{
         //console.log("you have no friends")
        }
        }
       
        );  
  }
  
  /**
   * afficher la liste de tout les utilisateurs
   * 
   */
    Users(){
      var code
      var url= new URLSearchParams();
      if(this.props.login!==null){
                 
      url.append('login',this.props.login)
      axios.get('http://localhost:8080/Life_Style/GetUsers?'+url)
        .then(response => {
          if(typeof response.data != 'undefined'){

            code=response.data["code"];
          if(code[0] === 200){
              this.setState({ListUsers: response.data["login"]})
  
          }
          else if(code === -1){
            store.addNotification({
              title: "Error!",
              message: "Error Server",
              type: 'error',
              insert: "top",
              container: "top-right",
              animationIn: ["animated", "fadeIn"],
              animationOut: ["animated", "fadeOut"],
              dismiss: {
                duration: 3000,
                onScreen: true
              }
            });
          }
         
        }});  
      }
   
  }
  getUserProfile(){
   alert("Hello")
      }
  usergetProfile(tmp){
    console.log(tmp)
    var l=this.state.listUsers
    for(var i=0;i<l.length;i++){
      if(tmp===l[i]){
        this.setState({
          userLoginprofile:l[i]
        })
      }
    }

    //this.setState({user_id:tmp})
  }
  /**
   * l'affichage de la page
   * selon la page séléctionné par l'utilisateur on retourne le composant correspondant
   * en passant par props tout les attributs adéquants
   */

    render() {
      var page
      
      if(this.props.page ==="Profile"){
       
        page = <Profile cle={this.props.cle}
                        setKey={this.props.setKey}
                        getLogin = {this.props.getLogin}
                        setLogin = {this.props.setLogin}
                        login={this.props.login}
                        setId={this.setId}
                        user_id={this.state.user_id}
                        lat={this.state.lat}
                        lng={this.state.lng}
                        tempMax={this.state.tempMax}
                        tempMin={this.state.tempMin}
                        img={this.state.img}
                        setImg={this.setImg}
                          />

      }
      else if(this.props.page ==="CreateEvent"){
        page = <CreateEvent cle={this.props.cle}
                          setKey={this.props.setKey}
                          getLogin = {this.props.getLogin}
                          setLogin = {this.props.setLogin}
                          login={this.props.login}
                          setId={this.setId}
                          user_id={this.state.user_id}
                          getHome = {this.props.getHome}
                          getProfile = {this.props.getProfile}
                          getCreateEvent ={this.props.getCreateEvent}
                          lat = {this.state.lat}
                          lng={this.state.lng}               
                         
          /> 

      }
      else if(this.props.page ==="Exercises"){
        page = <Exercises cle={this.props.cle}
                          setKey={this.props.setKey}
                          getLogin = {this.props.getLogin}
                          setLogin = {this.props.setLogin}
                          login={this.props.login}
                          setId={this.setId}
                          user_id={this.state.user_id}
                          getHome = {this.props.getHome}
                          getProfile = {this.props.getProfile}
          /> 

      }
      
 
      else if(this.props.page ==="Home"){

        page=<Home  cle= {this.props.cle} 
                    login={this.props.login}
                    setId={this.setId}
                    user_id={this.state.user_id}
                    getHome={this.props.getHome}
                    getUserProfile={this.props.getUserProfile}
                    page={this.props.page}
                    />
            
      }
      else if(this.props.page ==="Friends"){
       // console.log("Friends ControlePannel")
        page=<Friends cle= {this.props.cle} 
                      login={this.props.login}
                      getUserLogin={this.getUserLogin}
                      getUserProfile={this.props.getUserProfile}
                      setId={this.setId}
                      page = {this.props.page}
                      getFriends ={this.props.getFriends}
                    //user_id={this.state.user_id}
                    />
      }  
 
      else if(this.props.page==="UserProfile" ){
        var l=this.state.listUsers
        page=<UserProfile cle={this.props.cle}
                          page={this.props.page}
                          getUserProfile={this.props.getUserProfile}
                          userP={this.props.userP}
        />


      }
      else if(this.props.page==="notification"){
        page=<MyAlerts cle={this.props.cle}
                      login={this.props.login}
                      getHome = {this.props.getHome}
                      getProfile = {this.props.getProfile}
        />
      }
      var l1 = this.state.listNotification
      const mapNotif = l1.map(item =>  <Notifs
                                      login={this.props.login}
                                      cle = {this.props.cle}
                                      event_name ={item["event_name"]}
                                      date_rest ={item["date_rest"]}
                                      page={this.props.page}
                                   />
                                                  );   
      return(
      
          <div className="NavBar " key="Accueil" >
          <nav className="navbar navbar-expand-lg navbar-light bg-light menu">
           <NavItem className="navbar-brand titreStyle" >CouCouCLUB</NavItem>
            <div className="collapse navbar-collapse " id="navbarSupportedContent">
                
                <ul className="navbar-nav mr-auto ">
                    
                    <li className="nav-item">
                        <button  onClick={()=>this.handleHome()} className="btn btn-light liNav">Home</button>
                    </li>
 
                    <li className="nav-item ">
                        <button onClick={()=>this.handleProfile()} className="btn btn-light liNav" >Profile</button>
                    </li>
                    <li className="nav-item">
                        <button onClick={()=>this.handleFriends()} className="btn btn-light liNav" >Friends</button>
                    </li>
                  
                    <li className="nav-item">
                        <button  onClick={()=>this.handleCreateEvent()} className="btn btn-light liNav">New Events</button>
                    </li>
                    <li className="nav-item">
                        <button  onClick={()=>this.handleExercises()} className="btn btn-light liNav">Exercises</button>
                    </li>
                    <li className="nav-item signOut">
                        <button  onClick={()=>this.handleClick()} className="btn btn-outline-danger">Sign out</button>
                    </li>
                    
        
                </ul>
            </div>
         </nav>
         <div className="nav-item placeOf">
           <ul>
             <li className="visible">
               <span className="notnumb">{this.state.compRes}</span>Notifs
               <ul className="hidden">
                 {mapNotif}
               </ul>
             </li>
           </ul>
          </div>
         <div className="homeImg2">
           
               {/*} <div className="card " style={{ marginRight:'0'}}>
                     <img src={this.state.userimg} className="card-img-top fixSize" ></img>
                 </div>{*/}
               
                  <div className="weather" style={{ marginLeft:'0'}}>
                    <img src={this.state.imge} className="card-img-top fixSize" ></img>
                    <div className="cont">
                      <div className="card-text">Min temp:{this.state.tempMin}</div>
                      <div className="card-text">Max temp:{this.state.tempMax}</div>
                      <div className="card-text">Weather type:{this.state.description}</div>
                    </div>
                  </div>
                
          
            </div>
            <div>{page}</div>
         
        
        </div>                        
        );
    }
}
  
  export default Accueil;
