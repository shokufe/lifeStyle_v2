# Front-end du WebApp Life Style--version2

*Projet réalisé par :* 

   * Shokoufeh  AHMADI SIMAB
   * premiere version : https://gitlab.com/NacimaG/life_style


*Access au Back-V2:*
    [https://gitlab.com/shokufe/lifestyleBack-v2/]

*Base de données : le fichier lifeStyle1.sql

*Requirement :*\
    -ReactJs\
    -ServerApplication comme Tomcat9\
    -MySQl(mettre les configurations de votre BDD dans la classe BaseDD.DBStatic )\
    -Javaee\

*features ajoutée in V2 Front:*\
    - Notification:\
        + Ajoute la component de notification \
    - Fix le probleme de Refresh\
    - Ajoute le page de UserProfile
        + la possibilite de Follow en cas d'ils sont pas amie
        + la possibilite de unfollow en cas d'ils sont amie
        + la possibilite de voir plus d'info de person si ils sont amies
        + acces a ce page depuis la page de Friends
        + Notify les evenment proche d' aujourd'hui 
        
    
nouveaux features:
